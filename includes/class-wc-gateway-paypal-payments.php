<?php

add_action('woocommerce_paypal_payments_built_container', function ($c) {
    add_filter('signifyd_payment_details', function ($payment_details, $wc_order) use ($c) {
        if (!$wc_order instanceof WC_Order
            || 'ppcp-credit-card-gateway' !== $wc_order->get_payment_method()) {
            return $payment_details;
        }

        try {
            $order_id = $wc_order->get_meta('_ppcp_paypal_order_id');
            $order_endpoint = $c->get('api.endpoint.order');
            $order = $order_endpoint->order($order_id);
        } catch (Exception $exception) {
            return $payment_details;
        }

        return (object)[
            'card' => false,
            'avs_code' => null,
            'cvv_code' => null
        ];
    }, 10, 2);
});
