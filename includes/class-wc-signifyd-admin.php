<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WC_Signifyd_Admin')) {

    class WC_Signifyd_Admin
    {

        public static function init()
        {
            add_filter('bulk_actions-edit-shop_order', array(__CLASS__, 'add_signifyd_bulk_action'));
            add_filter('handle_bulk_actions-edit-shop_order', array(__CLASS__, 'handle_signifyd_create_case_bulk_action'), 10, 3);
            add_filter('manage_edit-shop_order_columns', array(__CLASS__, 'add_signifyd_columns'));
            add_filter('woocommerce_shop_order_list_table_columns', array(__CLASS__, 'add_signifyd_columns'));
            add_action('manage_shop_order_posts_custom_column', array(__CLASS__, 'render_signifyd_columns'), 10, 2);
            add_action('add_meta_boxes', array(__CLASS__, 'add_signifyd_meta_box'));
            add_filter('woocommerce_order_actions', array(__CLASS__, 'add_signifyd_order_action'));
            add_action('woocommerce_order_action_signifyd_create_case', array(__CLASS__, 'handle_signifyd_create_case_action'));
        }

        public static function add_signifyd_bulk_action($actions)
        {
            $actions['signifyd_create_case'] = __('Signifyd Create Case');

            return $actions;
        }

        public static function handle_signifyd_create_case_bulk_action($redirect_to, $action, $ids)
        {
            if ('signifyd_create_case' === $action) {
                foreach ($ids as $order_id) {
                    $order = wc_get_order($order_id);
                    if (!$order->get_meta('_signifyd_case_id') && apply_filters('signifyd_skip_create_case', false, $order) === false) {
                        $signifyd_api = new WC_Signifyd_API();
                        $signifyd_api->create_case($order);
                    }
                }
            }

            return $redirect_to;
        }

        public static function add_signifyd_columns($columns)
        {
            $columns['signifyd_guarantee_status'] = __('Guarantee Status');
            $columns['signifyd_case_score'] = __('Signifyd Score');

            return $columns;
        }

        public static function render_signifyd_columns($column, $post_id)
        {
            if ('signifyd_guarantee_status' === $column) {
                $order = wc_get_order($post_id);
                $signifyd_case_id = $order->get_meta('_signifyd_case_id');
                $signifyd_guarantee_status = $order->get_meta('_signifyd_guarantee_status');

                if ($signifyd_case_id && $signifyd_guarantee_status) {
                    echo sprintf('<a href="https://www.signifyd.com/cases/%s" target="_blank">%s</a>', $signifyd_case_id, $signifyd_guarantee_status);
                } else {
                    echo 'N/A';
                }
            } elseif ('signifyd_case_score' === $column) {
                $order = wc_get_order($post_id);
                $signifyd_case_id = $order->get_meta('_signifyd_case_id');
                $signifyd_case_score = $order->get_meta('_signifyd_case_score');

                if ($signifyd_case_id && $signifyd_case_score) {
                    echo sprintf('<a href="https://www.signifyd.com/cases/%s" target="_blank">%s</a>', $signifyd_case_id, $signifyd_case_score);
                } else {
                    echo 'N/A';
                }
            }
        }

        public static function add_signifyd_meta_box()
        {
            $order_screen_id = wc_get_page_screen_id( 'shop_order' );
            add_meta_box('admin_order_signifyd_status', __('Signifyd Status'), 'WC_Signifyd_Admin::admin_order_signifyd_status', $order_screen_id, 'side', 'default');
        }

        public static function admin_order_signifyd_status($post)
        {
            $order = wc_get_order($post->ID);
            $signifyd_case_id = $order->get_meta('_signifyd_case_id');
            $signifyd_case_score = $order->get_meta('_signifyd_case_score');
            $signifyd_guarantee_status = $order->get_meta('_signifyd_guarantee_status');

            if ($signifyd_case_id && $signifyd_guarantee_status) {
                echo sprintf('<p>Guarantee Status: <a href="https://www.signifyd.com/cases/%s" target="_blank">%s</a></p>', $signifyd_case_id, $signifyd_guarantee_status);
            } else {
                echo '<p>Guarantee Status: N/A</p>';
            }

            if ($signifyd_case_id && $signifyd_case_score) {
                echo sprintf('<p>Signifyd Score: <a href="https://www.signifyd.com/cases/%s" target="_blank">%s</a></p>', $signifyd_case_id, $signifyd_case_score);
            } else {
                echo '<p>Signifyd Score: N/A</p>';
            }
        }

        public static function add_signifyd_order_action($actions)
        {
            global $theorder;

            $supported_payment_gateways = WooCommerce_Signifyd()->get_supported_payment_gateways();

            if (!$theorder->get_meta('_signifyd_case_id') && in_array($theorder->get_payment_method(), $supported_payment_gateways)) {
                $actions['signifyd_create_case'] = __('Signifyd Create Case');
            }

            return $actions;
        }

        public static function handle_signifyd_create_case_action($order)
        {
            if (!$order->get_meta('_signifyd_case_id') && apply_filters('signifyd_skip_create_case', false, $order) === false) {
                $signifyd_api = new WC_Signifyd_API();
                $signifyd_api->create_case($order);
            }
        }

    }

}
