<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WC_Signifyd')) {

    final class WC_Signifyd
    {

        /**
         * @var WC_Signifyd
         */
        protected static $_instance = null;

        /**
         * @var string
         */
        public static $version = WC_SIGNIFYD_VERSION;

        /**
         * @var string
         */
        public static $supported_payment_gateways = null;

        /**
         * @var WC_Logger instance
         */
        public $logger;

        /**
         * @return WC_Signifyd instance
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }

        /**
         * WC_Signifyd Constructor
         */
        public function __construct()
        {
            self::$supported_payment_gateways = array(
                'authorize_net_aim' => 'Authorize.Net AIM (by SkyVerge)',
                'authorize_net_cim_credit_card' => 'Authorize.Net CIM (by SkyVerge)',
                'braintree_credit_card' => 'Braintree for WooCommerce Payment Gateway (by WooCommerce) v2.7.0',
                'braintree' => 'Braintree (by AngellEYE)',
                'ppec_paypal' => 'PayPal Checkout Gateway (by WooCommerce)',
                'paypal_pro' => 'PayPal Pro (by AngellEYE)',
                'paypal_pro_payflow' => 'PayPal Pro Payflow (by AngellEYE)',
                'paypal_credit_card_rest' => 'PayPal Credit Card REST (by AngellEYE)',
                'paypal_express' => 'PayPal Express Checkout (by AngellEYE)',
                'stripe' => 'Stripe (by WooCommerce)',
                'mes_cc' => 'Merchant e-Solutions - Credit Card',
                'cybsawm' => 'Cybersource (by Ryan IT Solutions) v1.35.28',
                'card_connect' => 'CardConnect (by CardConnect)',
                'first_data_payeezy_gateway_credit_card' => 'Payeezy Gateway Credit Card (by SkyVerge)',
                'moneris' => 'Moneris Gateway (by SkyVerge)',
                'square_credit_card' => 'WooCommerce Square (by SkyVerge)',
                'ppcp-gateway' => 'WooCommerce PayPal Payments (by WooCommerce) v1.6.4',
                'ppcp-credit-card-gateway' => 'WooCommerce PayPal Credit Card Payments (by WooCommerce) v1.6.4',
            );

            if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
                add_action('plugins_loaded', array($this, 'init'), 0);
            }
        }

        /**
         * Initialization hooks
         */
        public function init()
        {
            include_once dirname(__FILE__) . '/class-wc-signifyd-admin.php';
            include_once dirname(__FILE__) . '/class-wc-signifyd-api.php';
            include_once dirname(__FILE__) . '/class-wc-signifyd-integration.php';
            include_once dirname(__FILE__) . '/class-wc-signifyd-checkout-handler.php';
            include_once dirname(__FILE__) . '/class-wc-signifyd-webhook-event-handler.php';

            class_exists('CardConnectPaymentGateway') && include_once dirname(__FILE__) . '/class-wc-gateway-cardconnect.php';

            $this->logger = new WC_Logger();

            add_action('init', array($this, 'add_rewrite_rules'));
            add_filter('query_vars', array($this, 'add_query_vars'));
            add_action('template_redirect', array($this, 'handle_webhook_request'), 0);

            add_filter('plugin_action_links_' . WC_SIGNIFYD_BASENAME, array($this, 'add_plugin_links'));
            add_filter('woocommerce_integrations', array($this, 'add_integration'));

            add_action('woocommerce_init', function () {
                // check if plugin enabled
                if (!$this->is_enabled()) {
                    return;
                }

                if (is_admin()) {
                    WC_Signifyd_Admin::init();
                }

                // check current user role
                if (is_user_logged_in()) {
                    $user = wp_get_current_user();
                    $ignored_user_roles = $this->ignored_user_roles();

                    if (!empty(array_intersect($ignored_user_roles, $user->roles))) {
                        // user role is ignored
                        return;
                    }
                }

                WC_Signifyd_Checkout_Handler::init();

                add_action('wp_head', array($this, 'add_device_fingerprint_script'), 99);

                add_action('signifyd_webhook_request', array('WC_Signifyd_Webhook_Event_Handler', 'process_request'));

                add_action('signifyd_guarantee_status_update', array($this, 'on_guarantee_status_update'), 10, 2);
                add_action('signifyd_review_disposition_update', array($this, 'on_review_disposition_update'), 10, 2);
            });

            add_action('wp_enqueue_scripts', array($this, 'load_scripts'));
        }

        public static function on_activation()
        {
            update_option('rewrite_rules', '');
        }

        public static function on_deactivation()
        {
            update_option('rewrite_rules', '');
        }

        public function add_rewrite_rules()
        {
            add_rewrite_rule('^signifyd/connect/api/?$', 'index.php?signifyd_webhook=1', 'top');
        }

        public function add_query_vars($qvars)
        {
            $qvars[] = 'signifyd_webhook';

            return $qvars;
        }

        public function handle_webhook_request()
        {
            if (false === get_query_var('signifyd_webhook', false)) {
                return;
            }

            $post_data = file_get_contents('php://input');
            $hash = $_SERVER['HTTP_X_SIGNIFYD_SEC_HMAC_SHA256'] ?: '';
            $topic = $_SERVER['HTTP_X_SIGNIFYD_TOPIC'] ?: '';

            $signifyd_api = new WC_Signifyd_API();

            // validate request
            if (!$signifyd_api->get_client()->validWebhookRequest($post_data, $hash, $topic)) {
                wp_send_json_error('Request did not pass validation.', 403);
                exit;
            }

            // log webhook post data
            if ($this->is_logging_enabled()) {
                $this->logger->add('signifyd_webhook_event', $post_data, WC_Log_Levels::DEBUG);
            }

            try {
                do_action('signifyd_webhook_request', json_decode($post_data));

                wp_send_json_success('You have successfully reached the webhook endpoint.');
            } catch (Exception $e) {
                if ($this->is_logging_enabled()) {
                    $this->logger->add('signifyd_webhook_event', 'Exception in: ' . __FILE__ . ', on line: ' . __LINE__, WC_Log_Levels::ERROR);
                    $this->logger->add('signifyd_webhook_event', 'Exception:' . $e->__toString(), WC_Log_Levels::ERROR);
                }

                wp_send_json_error($e->getMessage(), 403);
            }

            exit;
        }

        public function add_device_fingerprint_script()
        {
            $session_cookie = WC()->session->get_session_cookie();
            if ($session_cookie) {
                echo sprintf('<script async type="text/javascript" id="sig-api" data-order-session-id="%s" src="%s"></script>', base64_encode(implode('-', $session_cookie)), '//cdn-scripts.signifyd.com/api/script-tag.js');
            }
        }

        public function add_plugin_links($links)
        {
            $settings_link = add_query_arg(array(
                'page' => 'wc-settings',
                'tab' => 'integration',
                'section' => 'signifyd'
            ), admin_url('admin.php'));

            $plugin_links = array(
                '<a href="' . $settings_link . '">' . __('Settings') . '</a>',
                '<a href="https://www.signifyd.com/resources/manual/overview/" target="_blank">' . __('Docs') . '</a>',
                '<a href="https://community.signifyd.com/support/" target="_blank">' . __('Support') . '</a>'
            );

            return array_merge($plugin_links, $links);
        }

        /**
         * Register Signifyd settings
         *
         * @return array
         */
        public function add_integration($integrations)
        {
            $integrations[] = 'WC_Signifyd_Integration';

            return $integrations;
        }

        public function load_scripts()
        {
            if (is_checkout()) {
                wp_enqueue_script('signifyd-checkout', WC_SIGNIFYD_DIR_URL . 'assets/checkout.js', array('wc-checkout'), WC_SIGNIFYD_VERSION, true);
            }
        }

        public function on_guarantee_status_update($guarantee_status, WC_Order $order)
        {
            // Do not change order status in test mode
            if ($this->is_test_mode()) {
                return;
            }

            // skip cancelled or completed orders
            if ($order->has_status(array('cancelled', 'completed'))) {
                return;
            }

            switch ($guarantee_status) {
                case 'APPROVED':
                    $new_status = $this->action_guarantee_approve();
                    $this->maybe_capture_payment($order);
                    break;
                case 'DECLINED':
                    $new_status = $this->action_guarantee_decline();
                    $this->maybe_refund_payment($order);
                    break;
                case 'PENDING':
                case 'CANCELED':
                case 'IN_REVIEW':
                    $new_status = false;
                    break;
                default:
                    $new_status = false;
            }

            if (false === $new_status || 'none' === $new_status) {
                return;
            }

            $order->set_status($new_status, 'Signifyd Guarantee status ' . $guarantee_status);
            $order->save();
        }

        public function on_review_disposition_update($review_disposition, WC_Order $order)
        {
            // Do not change order status in test mode
            if ($this->is_test_mode()) {
                return;
            }

            // skip cancelled or completed orders
            if ($order->has_status(array('cancelled', 'completed'))) {
                return;
            }

            switch ($review_disposition) {
                case 'GOOD':
                    $new_status = $this->action_guarantee_approve();
                    $this->maybe_capture_payment($order);
                    break;
                case 'FRAUDULENT':
                    $new_status = $this->action_guarantee_decline();
                    break;
                case 'UNSET':
                    $new_status = false;
                    break;
                default:
                    $new_status = false;
            }

            if (false === $new_status || 'none' === $new_status) {
                return;
            }

            $order->set_status($new_status, 'Signifyd Review disposition ' . $review_disposition);
            $order->save();
        }

        public function maybe_capture_payment(WC_Order $order)
        {
            try {

                $payment_gateway = wc_get_payment_gateway_by_order($order);

                switch ($order->get_payment_method()) {
                    case 'card_connect':
                        if ('capture' === $order->get_meta('_cardconnect_mode')
                            || !class_exists('SignifydCardConnectPaymentGateway')
                            || !empty($order->get_meta('_cardconnect_payment_captured'))) {
                            return;
                        }

                        $order->update_meta_data('_cardconnect_payment_captured', 1);
                        $order->save_meta_data();

                        $gateway = new SignifydCardConnectPaymentGateway();
                        $response = $gateway->capture_payment($order);

                        if (!empty($response['setlstat'])) {
                            $order->add_order_note('Payment capture status: ' . $response['setlstat']);
                        } else {
                            $order->add_order_note('Failed to capture payment');

                            $order->update_meta_data('_cardconnect_payment_captured', 0);
                            $order->save_meta_data();
                        }
                        break;

                    case 'first_data_payeezy_gateway_credit_card':
                        if (is_object($payment_gateway)
                            && method_exists($payment_gateway, 'get_capture_handler')) {
                            $payment_gateway->get_capture_handler()->maybe_perform_capture($order);
                        }
                        break;
                }

            } catch (Exception $e) {

                if ($this->is_logging_enabled()) {
                    $this->logger->add('signifyd', 'Exception in: ' . __FILE__ . ', on line: ' . __LINE__, WC_Log_Levels::ERROR);
                    $this->logger->add('signifyd', 'Exception:' . $e->__toString(), WC_Log_Levels::ERROR);
                }

            }
        }

        public function maybe_refund_payment(WC_Order $order)
        {
            try {

                if (!$this->is_order_refund_enabled()) return;

                $payment_gateway = wc_get_payment_gateway_by_order($order);

                if (is_object($payment_gateway) && method_exists($payment_gateway, 'process_refund')) {
                    $payment_gateway->process_refund($order->get_id(), $order->get_total(), 'Fraudulent transaction.');
                }

            } catch (Exception $e) {

                if ($this->is_logging_enabled()) {
                    $this->logger->add('signifyd', 'Exception in: ' . __FILE__ . ', on line: ' . __LINE__, WC_Log_Levels::ERROR);
                    $this->logger->add('signifyd', 'Exception:' . $e->__toString(), WC_Log_Levels::ERROR);
                }

            }
        }

        public function get_supported_payment_gateways($return_ids = true)
        {
            if ($return_ids) {
                return array_keys(self::$supported_payment_gateways);
            }

            return self::$supported_payment_gateways;
        }

        public function is_enabled()
        {
            $integrations = WC()->integrations->get_integrations();

            return 'no' !== $integrations['signifyd']->get_option('enabled');
        }

        public function is_test_mode()
        {
            $integrations = WC()->integrations->get_integrations();

            return 'test' === $integrations['signifyd']->get_option('enabled');
        }

        public function get_api_key()
        {
            $integrations = WC()->integrations->get_integrations();

            return $integrations['signifyd']->get_option('api_key');
        }

        public function is_logging_enabled()
        {
            $integrations = WC()->integrations->get_integrations();

            return 'yes' === $integrations['signifyd']->get_option('enable_logging');
        }

        public function action_guarantee_approve()
        {
            $integrations = WC()->integrations->get_integrations();

            return $integrations['signifyd']->get_option('action_guarantee_approve');
        }

        public function action_guarantee_decline()
        {
            $integrations = WC()->integrations->get_integrations();

            return $integrations['signifyd']->get_option('action_guarantee_decline');
        }

        public function ignored_user_roles()
        {
            $integrations = WC()->integrations->get_integrations();

            return $integrations['signifyd']->get_option('ignored_user_roles') ?: [];
        }

        public function delay_sending_order()
        {
            $integrations = WC()->integrations->get_integrations();

            return $integrations['signifyd']->get_option('delay_sending_order') ?: 0;
        }

        public function is_fulfillment_enabled()
        {
            $integrations = WC()->integrations->get_integrations();

            return 'no' !== $integrations['signifyd']->get_option('enable_fulfillment');
        }

        public function is_order_refund_enabled()
        {
            $integrations = WC()->integrations->get_integrations();

            return 'no' !== $integrations['signifyd']->get_option('enable_order_refund');
        }

    }

}
