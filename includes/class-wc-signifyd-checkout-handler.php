<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WC_Signifyd_Checkout_Handler')) {

    class WC_Signifyd_Checkout_Handler
    {

        public static $order;

        public static function init()
        {
            add_filter('woocommerce_checkout_posted_data', array(__CLASS__, 'add_cc_to_posted_data'));
            add_action('woocommerce_checkout_order_processed', array(__CLASS__, 'save_cc_posted_data'), 10, 3);
            add_action('woocommerce_checkout_order_processed', array(__CLASS__, 'save_session_id'), 10, 3);
            add_filter('woocommerce_payment_complete_order_status', array(__CLASS__, 'maybe_change_payment_complete_order_status'), 30, 3);

            add_action('woocommerce_payment_complete', array(__CLASS__, 'create_signifyd_case'));
            add_filter('woocommerce_payment_successful_result', function ($result, $order_id) {
                WC_Signifyd_Checkout_Handler::create_signifyd_case($order_id);

                return $result;
            }, 10, 2);
            add_action('action_scheduler/signifyd_create_case', array(__CLASS__, 'scheduled_create_signifyd_case'));

            /* Capture the CyberSource transaction response */
            add_action('woocommerce_api_wc_api_cybersource_sawm', array(__CLASS__, 'capture_cybsawm_response'), 0);
            /* Capture the PayPal transaction response */
            add_action('angelleye_paypal_response_data', array(__CLASS__, 'capture_paypal_response'));

            // SkyVerge payment plugins
            add_action('wc_payment_gateway_authorize_net_cim_credit_card_add_transaction_data', array(__CLASS__, 'capture_authorize_net_cim_credit_card_response'), 10, 2);
            add_action('wc_payment_gateway_first_data_payeezy_gateway_credit_card_add_transaction_data', array(__CLASS__, 'capture_first_data_payeezy_gateway_credit_card_response'), 10, 2);
            add_action('wc_payment_gateway_braintree_credit_card_add_transaction_data', array(__CLASS__, 'capture_braintree_credit_card_response'), 10, 2);
            add_action('wc_payment_gateway_moneris_add_transaction_data', array(__CLASS__, 'capture_moneris_response'), 10, 2);
            add_action('wc_payment_gateway_square_credit_card_add_transaction_data', array(__CLASS__, 'capture_square_credit_card_response'), 10, 2);

            /* Replace the initial CardConnect payment gateway class to capture requests */
            add_filter('woocommerce_payment_gateways', array(__CLASS__, 'replace_cardconnect_gateway'), 100);

            /* Stripe */
            add_filter('wc_stripe_allowed_payment_processing_statuses', array(__CLASS__, 'stripe_add_allowed_payment_processing_statuses'), 10, 2);

            /* Fulfillment */
            add_action('woocommerce_order_status_completed', array(__CLASS__, 'handle_fulfillment'));
        }

        /**
         * Adds on-hold as accepted status
         *
         * @param $allowed_statuses
         * @param $order
         *
         * @return mixed
         */
        public static function stripe_add_allowed_payment_processing_statuses($allowed_statuses, WC_Order $order)
        {
            $stripe_order_handler = WC_Stripe_Order_Handler::get_instance();
            $intent = $stripe_order_handler->get_intent_from_order($order);
            if ($intent && 'succeeded' === $intent->status) {
                $allowed_statuses[] = 'on-hold';
            }
            return $allowed_statuses;
        }

        /* Get the credit card field names for applicable payment methods  */
        public static function get_card_field_names($payment_method)
        {
            $field_names = array(
                'authorize_net_aim' => array(
                    'card' => 'wc-authorize-net-aim-account-number',
                    'date' => 'wc-authorize-net-aim-expiry'
                ),
                'paypal_pro' => array(
                    'card' => 'paypal_pro-card-number',
                    'month' => 'paypal_pro-card_expiration_month',
                    'year' => 'paypal_pro-card_expiration_year'
                ),
                'paypal_pro_payflow' => array(
                    'card' => 'paypal_pro_payflow-card-number',
                    'month' => 'paypal_pro_payflow-card_expiration_month',
                    'year' => 'paypal_pro_payflow-card_expiration_year'
                ),
                'paypal_credit_card_rest' => array(
                    'card' => 'paypal_credit_card_rest-card-number',
                    'date' => 'paypal_credit_card_rest-card-expiry',
                ),
                'mes_cc' => array(
                    'card' => 'mes_cc-card-number',
                    'date' => 'mes_cc-card-expiry'
                ),
                'moneris' => array(
                    'bin' => 'wc-moneris-card-bin'
                )
            );

            return (isset($field_names[$payment_method]) ? $field_names[$payment_method] : null);
        }

        /* Add the credit card number and expiry date to the checkout post data */

        public static function add_cc_to_posted_data($posted_data)
        {
            $payment_method = (isset($posted_data['payment_method']) ? wc_clean($posted_data['payment_method']) : false);
            $field_names = self::get_card_field_names($payment_method);

            if (empty($field_names)) {
                return $posted_data;
            }

            foreach ($field_names as $field_name) {
                if (!isset($posted_data[$field_name]) || empty($posted_data[$field_name])) {
                    $posted_data[$field_name] = isset($_REQUEST[$field_name]) ? wc_clean(wp_unslash($_REQUEST[$field_name])) : '';
                }
            }

            $posted_data['cc_number'] = '';
            $posted_data['cc_exp_month'] = '';
            $posted_data['cc_exp_year'] = '';

            if (!empty($field_names['card']) and !empty($posted_data[$field_names['card']])) {
                $posted_data['cc_number'] = wc_clean(preg_replace('/[^0-9]/', '', $posted_data[$field_names['card']]));
            } else if (!empty($field_names['bin']) and !empty($posted_data[$field_names['bin']])) {
                $posted_data['cc_bin'] = wc_clean(preg_replace('/[^0-9]/', '', $posted_data[$field_names['bin']]));
            }

            if (!empty($field_names['date']) and !empty($posted_data[$field_names['date']])) {
                $date = wc_clean($posted_data[$field_names['date']]);
                $parts = explode('/', $date);

                if (!empty($parts[0]) and !empty($parts[1])) {
                    $posted_data['cc_exp_month'] = intval(trim($parts[0]));
                    $posted_data['cc_exp_year'] = intval(trim($parts[1]));
                }

            } elseif (!empty($field_names['month']) and !empty($posted_data[$field_names['month']]) and !empty($field_names['year']) and !empty($posted_data[$field_names['year']])) {

                $posted_data['cc_exp_month'] = intval(trim($posted_data[$field_names['month']]));
                $posted_data['cc_exp_year'] = intval(trim($posted_data[$field_names['year']]));
            }

            return $posted_data;
        }

        /* Save the previously added credit card data to the order meta fields */
        public static function save_cc_posted_data($order_id, $posted_data, $order)
        {
            if (!$order instanceof WC_Order) {
                $order = wc_get_order($order_id);
            }

            $payment_method = $posted_data['payment_method'] ?: $order->get_payment_method();
            $supported_payment_gateways = WooCommerce_Signifyd()->get_supported_payment_gateways();

            if (!in_array($payment_method, $supported_payment_gateways)) {
                return;
            }

            if (isset($posted_data['cc_number']) && !empty($posted_data['cc_number'])) {
                $order->update_meta_data('_cc_bin', substr($posted_data['cc_number'], 0, 6));
                $order->update_meta_data('_cc_last4', substr($posted_data['cc_number'], -4));
            } else if (isset($posted_data['cc_bin']) && !empty($posted_data['cc_bin'])) {
                $order->update_meta_data('_cc_bin', $posted_data['cc_bin']);
            }

            if (isset($posted_data['cc_exp_month']) && !empty($posted_data['cc_exp_month'])) {
                $order->update_meta_data('_cc_exp_month', sprintf('%02d', $posted_data['cc_exp_month']));
            }

            if (isset($posted_data['cc_exp_year']) && !empty($posted_data['cc_exp_year'])) {
                $order->update_meta_data('_cc_exp_year', sprintf('20%02d', substr($posted_data['cc_exp_year'], -2)));
            }

            $order->save_meta_data();
        }

        public static function save_session_id($order_id, $posted_data, $order)
        {
            if (!$order instanceof WC_Order) {
                $order = wc_get_order($order_id);
            }

            $session_cookie = WC()->session->get_session_cookie();

            if ($session_cookie) {
                $session_id = base64_encode(implode('-', $session_cookie));
                $order->update_meta_data('_order_session_id', $session_id);
                $order->save_meta_data();
            }

            /* Capture the current order for further processing */
            self::$order = $order;
        }

        public static function maybe_change_payment_complete_order_status($status, $order_id, $order)
        {
            if (!$order instanceof WC_Order) {
                $order = wc_get_order($order_id);
            }

            $payment_method = $order->get_payment_method();
            $supported_payment_gateways = WooCommerce_Signifyd()->get_supported_payment_gateways();

            if ($order->get_meta('_signifyd_case_id')
                || !in_array($payment_method, $supported_payment_gateways)
                || WooCommerce_Signifyd()->is_test_mode()) {
                return $status;
            }

            return 'on-hold';
        }

        public static function create_signifyd_case($order_id)
        {
            $order = wc_get_order($order_id);
            $payment_method = $order->get_payment_method();
            $supported_payment_gateways = WooCommerce_Signifyd()->get_supported_payment_gateways();

            // check if payment method is supported
            if ($order->get_meta('_signifyd_case_id')
                || !in_array($payment_method, $supported_payment_gateways)
                || $order->needs_payment()
                || apply_filters('signifyd_skip_create_case', false, $order)) {
                return;
            }

            $payment_details = self::get_payment_details($order);
            if (is_wp_error($payment_details)) {
                remove_filter('woocommerce_payment_complete_order_status', array(__CLASS__, 'maybe_change_payment_complete_order_status'), 30);

                $order->set_status(apply_filters('woocommerce_payment_complete_order_status', $order->needs_processing() ? 'processing' : 'completed', $order->get_id(), $order));
                $order->save();

                return;
            }

            // maybe schedule sending order
            $delay_sending_order = WooCommerce_Signifyd()->delay_sending_order() ?: 0;
            if ($delay_sending_order > 0 && function_exists('as_schedule_single_action')) {
                $scheduled_action = 'action_scheduler/signifyd_create_case';
                $scheduled_action_args = array('order_id' => $order_id);

                // action has been scheduled already
                if (false !== as_next_scheduled_action($scheduled_action, $scheduled_action_args)) {
                    return;
                }

                as_schedule_single_action(gmdate('U') + ($delay_sending_order * MINUTE_IN_SECONDS), $scheduled_action, $scheduled_action_args);

                return;
            }

            $signifyd_api = new WC_Signifyd_API();
            $signifyd_api->create_case($order);
        }

        public static function scheduled_create_signifyd_case($order_id)
        {
            $order = wc_get_order($order_id);
            $supported_payment_gateways = WooCommerce_Signifyd()->get_supported_payment_gateways();

            if ($order->get_meta('_signifyd_case_id')
                || !in_array($order->get_payment_method(), $supported_payment_gateways)
                || $order->needs_payment()
                || apply_filters('signifyd_skip_create_case', false, $order)) {
                return;
            }

            $signifyd_api = new WC_Signifyd_API();
            $signifyd_api->create_case($order);
        }

        public static function get_payment_details($order)
        {
            $payment_details = null;

            try {

                switch ($order->get_payment_method()) {
                    case 'authorize_net_aim':
                        $payment_details = self::get_authorize_net_payment_details($order);
                        break;
                    case 'braintree':
                        $payment_details = self::get_braintree_payment_details($order);
                        break;
                    case 'ppec_paypal':
                    case 'ppcp-gateway':
                    case 'ppcp-credit-card-gateway':
                        $payment_details = self::get_paypal_payment_details($order);
                        break;
                    case 'paypal_express':
                        $payment_details = self::get_paypal_express_payment_details($order);
                        break;
                    case 'paypal_pro':
                        $payment_details = self::get_paypal_pro_payment_details($order);
                        break;
                    case 'paypal_pro_payflow':
                        $payment_details = self::get_paypal_pro_payflow_payment_details($order);
                        break;
                    case 'stripe':
                        $payment_details = self::get_stripe_payment_details($order);
                        break;
                    case 'mes_cc':
                        $payment_details = self::get_mes_cc_payment_details($order);
                        break;
                    case 'authorize_net_cim_credit_card':
                    case 'braintree_credit_card':
                    case 'card_connect':
                    case 'cybsawm':
                    case 'first_data_payeezy_gateway_credit_card':
                    case 'moneris':
                    case 'paypal_credit_card_rest':
                    case 'square_credit_card':
                        $payment_details = self::get_saved_payment_details($order);
                        break;
                }

                $payment_details = apply_filters('signifyd_payment_details', $payment_details, $order);

            } catch (Exception $e) {
                if (WooCommerce_Signifyd()->is_logging_enabled()) {
                    WooCommerce_Signifyd()->logger->add('signifyd', 'Exception in: ' . __FILE__ . ', on line: ' . __LINE__, WC_Log_Levels::ERROR);
                    WooCommerce_Signifyd()->logger->add('signifyd', 'Exception:' . $e->__toString(), WC_Log_Levels::ERROR);
                }

                return new WP_Error('signifyd', $e->getMessage());
            }

            if (empty($payment_details)) {
                return new WP_Error('signifyd', 'Missing payment details.');
            }

            if (false !== $payment_details->card) {
                $order->update_meta_data('_cc_bin', $payment_details->card->bin);
                $order->update_meta_data('_cc_last4', $payment_details->card->last4);
                $order->update_meta_data('_cc_exp_month', sprintf('%02d', $payment_details->card->exp_month));
                $order->update_meta_data('_cc_exp_year', sprintf('20%02d', substr($payment_details->card->exp_year, -2)));
            }

            $order->update_meta_data('_avs_code', $payment_details->avs_code);
            $order->update_meta_data('_cvv_code', $payment_details->cvv_code);
            $order->save_meta_data();

            return $payment_details;
        }

        /**
         * Return previously saved payment details
         */
        public static function get_saved_payment_details(WC_Order $order)
        {
            return (object)[
                'card' => false,
                'avs_code' => $order->get_meta('_avs_code'),
                'cvv_code' => $order->get_meta('_cvv_code')
            ];
        }

        public static function get_authorize_net_payment_details(WC_Order $order)
        {
            $payment_gateway = wc_get_payment_gateway_by_order($order);

            if (is_object($payment_gateway) and method_exists($payment_gateway, 'get_api')) {
                $transaction = $payment_gateway->get_api()->get_transaction_details($order->get_transaction_id());

                if (!$transaction->has_api_error()) {
                    return (object)[
                        'card' => false,// card details already saved
                        'avs_code' => ('B' == $transaction->transaction->AVSResponse ? 'U' : $transaction->transaction->AVSResponse),
                        'cvv_code' => $transaction->transaction->cardCodeResponse
                    ];
                }

            }

            return null;
        }

        public static function get_braintree_payment_details(WC_Order $order)
        {
            $payment_gateway = wc_get_payment_gateway_by_order($order);
            $payment_gateway->angelleye_braintree_lib($order->get_id());

            $transaction = $payment_gateway->braintree_gateway->transaction()->find($order->get_transaction_id());
            $payment_type = strtoupper($transaction->paymentInstrumentType);

            if (in_array($payment_type, ['PAYPAL_ACCOUNT', 'PAYPAL_HERE', 'US_BANK_ACCOUNT', 'VENMO_ACCOUNT'])) {
                return null;
            }

            $avs_mapping = array('MM' => 'Y', 'MN' => 'Z', 'MU' => 'Z', 'MI' => 'Z', 'NM' => 'A', 'NN' => 'N', 'NU' => 'N', 'NI' => 'N', 'UU' => 'U', 'II' => 'U', 'AA' => 'U', 'S' => 'G', 'E' => 'U', 'A' => 'U', 'B' => 'U');
            $cvv_mapping = array('M' => 'M', 'N' => 'N', 'U' => 'P', 'I' => 'P', 'S' => 'S', 'A' => null, 'B' => 'P');

            $payment_details = (object)[
                'card' => false,
                'avs_code' => !is_null($transaction->avsErrorResponseCode) ? $avs_mapping[$transaction->avsErrorResponseCode] : $avs_mapping[$transaction->avsStreetAddressResponseCode . $transaction->avsPostalCodeResponseCode],
                'cvv_code' => $cvv_mapping[$transaction->cvvResponseCode] ?: null
            ];

            switch ($payment_type) {
                case 'ANDROID_PAY_CARD':
                    $payment_details->card = (object)[
                        'bin' => $transaction->androidPayCardDetails->bin,
                        'last4' => $transaction->androidPayCardDetails->sourceCardLast4,
                        'exp_month' => $transaction->androidPayCardDetails->expirationMonth,
                        'exp_year' => $transaction->androidPayCardDetails->expirationYear
                    ];
                    break;
                case 'APPLE_PAY_CARD':
                    $payment_details->card = (object)[
                        'bin' => $transaction->applePayCardDetails->bin,
                        'last4' => $transaction->applePayCardDetails->last4,
                        'exp_month' => $transaction->applePayCardDetails->expirationMonth,
                        'exp_year' => $transaction->applePayCardDetails->expirationYear
                    ];
                    break;
                case 'CREDIT_CARD':
                    $payment_details->card = (object)[
                        'bin' => $transaction->creditCardDetails->bin,
                        'last4' => $transaction->creditCardDetails->last4,
                        'exp_month' => $transaction->creditCardDetails->expirationMonth,
                        'exp_year' => $transaction->creditCardDetails->expirationYear
                    ];
                    break;
                case 'MASTERPASS_CARD':
                    $payment_details->card = (object)[
                        'bin' => $transaction->masterpassCardDetails->bin,
                        'last4' => $transaction->masterpassCardDetails->last4,
                        'exp_month' => $transaction->masterpassCardDetails->expirationMonth,
                        'exp_year' => $transaction->masterpassCardDetails->expirationYear
                    ];
                    break;
                case 'SAMSUNG_PAY_CARD':
                    $payment_details->card = (object)[
                        'bin' => $transaction->samsungPayCardDetails->bin,
                        'last4' => $transaction->samsungPayCardDetails->sourceCardLast4,
                        'exp_month' => $transaction->samsungPayCardDetails->expirationMonth,
                        'exp_year' => $transaction->samsungPayCardDetails->expirationYear
                    ];
                    break;
                case 'VISA_CHECKOUT_CARD':
                    $payment_details->card = (object)[
                        'bin' => $transaction->visaCheckoutCardDetails->bin,
                        'last4' => $transaction->visaCheckoutCardDetails->sourceCardLast4,
                        'exp_month' => $transaction->visaCheckoutCardDetails->expirationMonth,
                        'exp_year' => $transaction->visaCheckoutCardDetails->expirationYear
                    ];
                    break;
            }

            return $payment_details;
        }

        public static function get_paypal_payment_details(WC_Order $order)
        {
            return (object)[
                'card' => false,
                'avs_code' => null,
                'cvv_code' => null
            ];
        }

        public static function get_paypal_express_payment_details(WC_Order $order)
        {
            return (object)[
                'card' => false,
                'avs_code' => null,
                'cvv_code' => null
            ];
        }

        public static function get_paypal_pro_payment_details(WC_Order $order)
        {
            $avs_mapping = array('A' => 'A', 'B' => 'B', 'C' => 'I', 'D' => 'D', 'E' => 'S', 'F' => 'M', 'G' => 'G', 'I' => 'G', 'M' => 'Y', 'N' => 'N', 'P' => 'P', 'R' => 'R', 'S' => 'S', 'U' => 'U', 'W' => 'W', 'X' => 'X', 'Y' => 'Y', 'Z' => 'Z');
            $cvv_mapping = array('E' => null, 'I' => null, 'M' => 'M', 'N' => 'N', 'P' => 'P', 'S' => 'N', 'U' => 'U', 'X' => 'N');

            return (object)[
                'card' => false,// card details already saved
                'avs_code' => $avs_mapping[$order->get_meta('_AVSCODE')] ?: 'E',
                'cvv_code' => $cvv_mapping[$order->get_meta('_CVV2MATCH')] ?: null
            ];
        }

        public static function get_paypal_pro_payflow_payment_details(WC_Order $order)
        {
            $avs_mapping = array('YN' => 'A', 'NN' => 'N', 'XX' => 'U', 'YY' => 'Y', 'NY' => 'Z');
            $cvv_mapping = array('Y' => 'M', 'N' => 'N', 'X' => 'U');

            return (object)[
                'card' => false,// card details already saved
                'avs_code' => $avs_mapping[$order->get_meta('_AVSADDR') . $order->get_meta('_AVSZIP')] ?: 'U',
                'cvv_code' => $cvv_mapping[$order->get_meta('_CVV2MATCH')] ?: 'U'
            ];
        }

        public static function get_stripe_payment_details(WC_Order $order)
        {
            $charge = null;
            $stripe_order_handler = WC_Stripe_Order_Handler::get_instance();
            $intent = $stripe_order_handler->get_intent_from_order($order);
            if ($intent) {
                $charge = $stripe_order_handler->get_latest_charge_from_intent($intent);
            } else if ($order->get_transaction_id()) {
                $charge = WC_Stripe_API::retrieve('charges/' . $order->get_transaction_id());
            }

            if (empty($charge)) {
                return false;
            }

            $avs_mapping = array('pp' => 'Y', 'pf' => 'A', 'fp' => 'Z', 'ff' => 'N', 'pu' => 'A', 'up' => 'Z', 'uf' => 'N', 'fu' => 'N', 'uu' => 'U');
            $cvv_mapping = array('pass' => 'M', 'fail' => 'N', 'unchecked' => 'P', 'unavailable' => 'P');

            return (object)[
                'card' => (object)[
                    'bin' => null,
                    'last4' => $charge->payment_method_details->card->last4,
                    'exp_month' => $charge->payment_method_details->card->exp_month,
                    'exp_year' => $charge->payment_method_details->card->exp_year
                ],
                'avs_code' => $avs_mapping[substr($charge->payment_method_details->card->checks->address_line1_check, 0, 1) . substr($charge->payment_method_details->card->checks->address_postal_code_check, 0, 1)] ?: 'U',
                'cvv_code' => $cvv_mapping[$charge->payment_method_details->card->checks->cvc_check] ?: 'P'
            ];
        }

        /**
         * @see https://merchante-solutions.app.box.com/v/MeS-Gateway-API-Doc-Latest
         */
        public static function get_mes_cc_payment_details(WC_Order $order)
        {
            return (object)[
                'card' => false,// card details already saved
                'avs_code' => $order->get_meta('avs_result'),
                'cvv_code' => $order->get_meta('cvv2_result')
            ];
        }

        /**
         * Capture the CyberSource response with payment details after user has made a payment on the payment page
         */
        public static function capture_cybsawm_response()
        {
            // All response fields are described here: https://developer.cybersource.com/library/documentation/dev_guides/Secure_Acceptance_Hosted_Checkout/Secure_Acceptance_Hosted_Checkout.pdf (page 132 and further)
            $request = !empty($_REQUEST) ? $_REQUEST : false;

            if (!$request || empty($request['req_transaction_uuid']) || 'ACCEPT' !== $request['decision']) {
                return;
            }

            $transaction_uuid = wc_clean($request['req_transaction_uuid']);

            $order_id = explode('-', $transaction_uuid);
            if (empty($order_id[1])) {
                return;
            }

            $order = wc_get_order(intval($order_id[1]));
            if ($order instanceof WC_Order) {
                return;
            }

            if (!empty($request['req_card_number'])) {
                $card_number = wc_clean($request['req_card_number']);
                $order->update_meta_data('_cc_bin', substr($card_number, 0, 6));
                $order->update_meta_data('_cc_last4', substr($card_number, -4));
            }

            if (!empty($request['req_card_expiry_date'])) {
                $card_expiry_date = explode('-', wc_clean($request['req_card_expiry_date']));
                $order->update_meta_data('_cc_exp_month', sprintf('%02d', $card_expiry_date[0]));
                $order->update_meta_data('_cc_exp_year', sprintf('20%02d', substr($card_expiry_date[1], -2)));
            }

            // Set the AVS code. Signifyd and CyberSource are using the same AVS codes. See pages 172-174
            if (!empty($request['auth_avs_code'])) {
                $avs_code = strtoupper(wc_clean($request['auth_avs_code']));
                $order->update_meta_data('_avs_code', $avs_code);
            }

            // Set the CVN/CVV code. Signifyd and CyberSource are using different codes. See page 175
            if (!empty($request['auth_cv_result'])) {
                $mapping = array(
                    'D' => 'U',
                    'I' => 'N',
                    'M' => 'M',
                    'N' => 'N',
                    'P' => 'P',
                    'U' => 'S',
                    'X' => 'S',
                    '1' => 'S',
                    '2' => 'U',
                    '3' => 'U',
                );

                $cv_result = strtoupper(wc_clean($request['auth_cv_result']));
                $cvv_code = isset($mapping[$cv_result]) ? $mapping[$cv_result] : 'P';
                $order->update_meta_data('_cvv_code', $cvv_code);
            }

            $order->save_meta_data();
        }

        /**
         * Capture the PayPal REST response and store the CVV and AVS codes in the order metadata
         */
        public static function capture_paypal_response($sale)
        {
            if (self::$order instanceof WC_Order and is_object($sale) and !empty($sale->processor_response) and $sale->processor_response instanceof \PayPal\Api\ProcessorResponse) {

                // See details here: https://www.signifyd.com/resources/manual/avs-and-cvv-mapping-guide/#paypal
                $avs_code = strtoupper($sale->processor_response->getAvsCode());
                $cvv_code = strtoupper($sale->processor_response->getCvvCode());

                $avs_mapping = array('A' => 'A', 'B' => 'B', 'C' => 'I', 'D' => 'D', 'E' => 'S', 'F' => 'M', 'G' => 'G', 'I' => 'G', 'M' => 'Y', 'N' => 'N', 'P' => 'P', 'R' => 'R', 'S' => 'S', 'U' => 'U', 'W' => 'W', 'X' => 'X', 'Y' => 'Y', 'Z' => 'Z');

                $avs_code = (isset($avs_mapping[$avs_code]) ? $avs_mapping[$avs_code] : $avs_code);

                self::$order->update_meta_data('_avs_code', $avs_code);
                self::$order->update_meta_data('_cvv_code', $cvv_code);
                self::$order->save_meta_data();
            }
        }

        public static function capture_authorize_net_cim_credit_card_response(WC_Order $order, $response)
        {
            if (empty($response) || !$response instanceof WC_Authorize_Net_CIM_API_Transaction_Response) {
                return;
            }

            $avs_mapping = array('B' => 'U');

            $avs_code = strtoupper($response->get_avs_result());
            $cvv_code = strtoupper($response->get_csc_result());

            $avs_code = (isset($avs_mapping[$avs_code]) ? $avs_mapping[$avs_code] : $avs_code);

            $order->update_meta_data('_avs_code', $avs_code);
            $order->update_meta_data('_cvv_code', $cvv_code);

            if (is_object($order->payment) && !empty($order->payment)) {
                $order->update_meta_data('_cc_bin', '');// bin not returned
                $order->update_meta_data('_cc_last4', $order->payment->last_four);
                $order->update_meta_data('_cc_exp_month', $order->payment->exp_month);
                $order->update_meta_data('_cc_exp_year', $order->payment->exp_year);
            }

            $order->save_meta_data();
        }

        public static function capture_first_data_payeezy_gateway_credit_card_response(WC_Order $order, $response)
        {
            if (empty($response)) {
                return;
            }

            $avs_mapping = array('W' => 'Z', 'E' => null, 'Q' => 'U', 'C' => 'I', '1' => 'N', '2' => 'Y', '3' => 'Z', '4' => 'A', '5' => 'Y', '6' => 'Z', '7' => 'A', '8' => 'N', 'F' => 'M', 'M' => 'Y');
            $cvv_mapping = array('S' => 'U', 'I' => 'N', 'Z' => 'P');

            $avs_code = strtoupper($response->get_avs_result());
            $cvv_code = strtoupper($response->get_csc_result());

            $avs_code = (isset($avs_mapping[$avs_code]) ? $avs_mapping[$avs_code] : $avs_code);
            $cvv_code = (isset($cvv_mapping[$cvv_code]) ? $cvv_mapping[$cvv_code] : $cvv_code);

            $order->update_meta_data('_avs_code', $avs_code);
            $order->update_meta_data('_cvv_code', $cvv_code);

            if (is_object($order->payment) && !empty($order->payment)) {
                $order->update_meta_data('_cc_bin', substr($order->payment->account_number, 0, 6));
                $order->update_meta_data('_cc_last4', $order->payment->last_four);
                $order->update_meta_data('_cc_exp_month', $order->payment->exp_month);
                $order->update_meta_data('_cc_exp_year', $order->payment->exp_year);
            }

            $order->save_meta_data();
        }

        public static function capture_braintree_credit_card_response(WC_Order $order, $response)
        {
            if (empty($response)) {
                return;
            }

            if (in_array(strtolower($order->get_created_via()), ['apple_pay', 'google_pay'])) {
                return;
            }

            $avs_mapping = array('MM' => 'Y', 'MN' => 'Z', 'MU' => 'Z', 'MI' => 'Z', 'NM' => 'A', 'NN' => 'N', 'NU' => 'N', 'NI' => 'N', 'UU' => 'U', 'II' => 'U', 'AA' => 'U', 'S' => 'G', 'E' => 'U', 'A' => 'U', 'B' => 'U');
            $cvv_mapping = array('M' => 'M', 'N' => 'N', 'U' => 'P', 'I' => 'P', 'S' => 'S', 'A' => null, 'B' => 'P');

            $avs_code = str_replace(':', '', strtoupper($response->get_avs_result()));
            $cvv_code = strtoupper($response->get_csc_result());

            $avs_code = (isset($avs_mapping[$avs_code]) ? $avs_mapping[$avs_code] : $avs_code);
            $cvv_code = (isset($cvv_mapping[$cvv_code]) ? $cvv_mapping[$cvv_code] : $cvv_code);

            $order->update_meta_data('_avs_code', $avs_code);
            $order->update_meta_data('_cvv_code', $cvv_code);

            $order->update_meta_data('_cc_bin', $response->get_bin());
            $order->update_meta_data('_cc_last4', $response->get_last_four());
            $order->update_meta_data('_cc_exp_month', $response->get_exp_month());
            $order->update_meta_data('_cc_exp_year', $response->get_exp_year());

            $order->save_meta_data();
        }

        public static function capture_moneris_response(WC_Order $order, $response)
        {
            if (empty($response)) {
                return;
            }

            $order->update_meta_data('_avs_code', $response->get_avs_result());
            $order->update_meta_data('_cvv_code', $response->get_csc_result());

            if (is_object($order->payment) && !empty($order->payment)) {
                $order->update_meta_data('_cc_bin', substr($order->payment->account_number, 0, 6));
                $order->update_meta_data('_cc_last4', substr($order->payment->account_number, -4));
                $order->update_meta_data('_cc_exp_month', $order->payment->exp_month);
                $order->update_meta_data('_cc_exp_year', $order->payment->exp_year);
            }

            $order->save_meta_data();
        }

        public static function capture_square_credit_card_response(WC_Order $order, $response)
        {
            if (empty($response)) {
                return;
            }

            $avs_code = $response->get_payment()['card_details']->getAvsStatus() ?: 'AVS_NOT_CHECKED';
            $cvv_code = $response->get_payment()['card_details']->getCvvStatus() ?: 'CVV_NOT_CHECKED';

            $avs_mapping = array('AVS_ACCEPTED' => 'Y', 'AVS_REJECTED' => 'N', 'AVS_NOT_CHECKED' => 'U');
            $cvv_mapping = array('CVV_ACCEPTED' => 'M', 'CVV_REJECTED' => 'N', 'CVV_NOT_CHECKED' => 'P');

            $avs_code = (isset($avs_mapping[$avs_code]) ? $avs_mapping[$avs_code] : $avs_code);
            $cvv_code = (isset($cvv_mapping[$cvv_code]) ? $cvv_mapping[$cvv_code] : $cvv_code);

            $order->update_meta_data('_avs_code', $avs_code);
            $order->update_meta_data('_cvv_code', $cvv_code);

            if (is_object($order->payment) && !empty($order->payment)) {
                $order->update_meta_data('_cc_bin', '');
                $order->update_meta_data('_cc_last4', substr($order->payment->account_number, -4));
                $order->update_meta_data('_cc_exp_month', $order->payment->exp_month);
                $order->update_meta_data('_cc_exp_year', $order->payment->exp_year);
            }

            $order->save_meta_data();
        }

        /**
         * Replace the CardConnect payment gateway to capture the transaction data
         */
        public static function replace_cardconnect_gateway($methods)
        {
            if (is_array($methods) and in_array('CardConnectPaymentGateway', $methods)) {
                $methods = array_filter($methods, function ($method) {
                    return $method !== 'CardConnectPaymentGateway';
                });
                $methods[] = 'SignifydCardConnectPaymentGateway';
            }

            return $methods;
        }

        public static function handle_fulfillment($order_id)
        {
            if (!function_exists('wc_shipment_tracking') || !WooCommerce_Signifyd()->is_fulfillment_enabled()) {
                return;
            }

            try {
                $order = wc_get_order($order_id);

                $wc_shipment_tracking = wc_shipment_tracking();
                $st = $wc_shipment_tracking->actions;

                $order_items = [];
                foreach ($order->get_items('line_item') as $item) {
                    $order_items[] = [
                        'itemName' => $item->get_name(),
                        'itemQuantity' => $item->get_quantity()
                    ];
                }

                $date_shipped = null;
                $tracking_providers = [];
                $tracking_numbers = [];
                $tracking_urls = [];
                foreach ($st->get_tracking_items($order_id, true) as $item) {
                    if (!$date_shipped) $date_shipped = $item['date_shipped'];
                    $tracking_providers[] = $item['formatted_tracking_provider'];
                    $tracking_numbers[] = $item['tracking_number'];
                    $tracking_urls[] = $item['formatted_tracking_link'];
                }

                $data = [
                    'orderId' => $order->get_order_number(),
                    'fulfillmentStatus' => 'COMPLETE',
                    'fulfillments' => [
                        [
                            'shipmentId' => 'shipment_' . $order_id,
                            'shippedAt' => gmdate('c', $date_shipped),
                            'products' => $order_items,
                            'shipmentStatus' => 'IN_TRANSIT',
                            'trackingUrls' => $tracking_urls,
                            'trackingNumbers' => $tracking_numbers,
                            'destination' => [
                                'fullName' => $order->get_formatted_shipping_full_name(),
                                'organization' => $order->get_shipping_company(),
                                'address' => [
                                    'streetAddress' => $order->get_shipping_address_1(),
                                    'unit' => $order->get_shipping_address_2(),
                                    'postalCode' => $order->get_shipping_postcode(),
                                    'city' => $order->get_shipping_city(),
                                    'provinceCode' => $order->get_shipping_state(),
                                    'countryCode' => $order->get_shipping_country()
                                ],
                                'confirmationPhone' => $order->get_shipping_phone()
                            ],
                            'carrier' => join(',', array_unique($tracking_providers)),
                            'fulfillmentMethod' => 'STANDARD_SHIPPING'
                        ]
                    ]
                ];

                $curl = curl_init();
                $curl_options = [
                    CURLOPT_URL => 'https://api.signifyd.com/v3/orders/events/fulfillments',
                    CURLOPT_USERPWD => WooCommerce_Signifyd()->get_api_key() . ':Signifyd',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => [
                        'Content-Type: application/json',
                    ]
                ];
                curl_setopt_array($curl, $curl_options);

                $response = curl_exec($curl);
                curl_close($curl);

                if (WooCommerce_Signifyd()->is_logging_enabled()) {
                    WooCommerce_Signifyd()->logger->add('signifyd_fulfillment', json_encode($curl_options), WC_Log_Levels::DEBUG);
                    WooCommerce_Signifyd()->logger->add('signifyd_fulfillment', $response, WC_Log_Levels::DEBUG);
                }
            } catch (Exception $e) {
                if (WooCommerce_Signifyd()->is_logging_enabled()) {
                    WooCommerce_Signifyd()->logger->add('signifyd_fulfillment', 'Exception in: ' . __FILE__ . ', on line: ' . __LINE__, WC_Log_Levels::ERROR);
                    WooCommerce_Signifyd()->logger->add('signifyd_fulfillment', 'Exception:' . $e->__toString(), WC_Log_Levels::ERROR);
                }
            }
        }
    }

}
