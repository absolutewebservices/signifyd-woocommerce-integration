<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

use Signifyd\Core\SignifydAPI;
use Signifyd\Core\SignifydSettings;
use Signifyd\Models\CaseModel;
use Signifyd\Models\Purchase;
use Signifyd\Models\Recipient;
use Signifyd\Models\Card;
use Signifyd\Models\UserAccount;
use Signifyd\Models\Address;
use Signifyd\Models\Shipment;
use Signifyd\Models\Product;

if (!class_exists('WC_Signifyd_API')) {

    class WC_Signifyd_API
    {

        private $client;
        private $logger;

        public function __construct()
        {
            $client_settings = new SignifydSettings();
            $client_settings->apiKey = WooCommerce_Signifyd()->get_api_key();
            $client_settings->logErrors = WooCommerce_Signifyd()->is_logging_enabled();
            $client_settings->logInfo = WooCommerce_Signifyd()->is_logging_enabled();
            $client_settings->logWarnings = WooCommerce_Signifyd()->is_logging_enabled();
            $client_settings->logFileLocation = untrailingslashit(WC_LOG_DIR);
            $client_settings->logFileName = sprintf('signifyd_api-%s.log', date('Y-m-d'));

            $this->client = new SignifydAPI($client_settings);
            $this->logger = WooCommerce_Signifyd()->is_logging_enabled() ? WooCommerce_Signifyd()->logger : false;
        }

        public function get_client()
        {
            return $this->client;
        }

        public function get_case($case_id)
        {
            return $this->client->getCase($case_id);
        }

        public function create_case($order)
        {
            try {

                do_action('signifyd_api_before_create_case', $order);

                $case = new CaseModel();
                $case->purchase = $this->get_purchase($order) ?: null;
                $case->recipient = $this->get_recipient($order) ?: null;
                $case->card = $this->get_card($order) ?: null;
                $case->userAccount = $this->get_user_account($order) ?: null;

                $case->clientVersion = new stdClass;
                $case->clientVersion->storePlatform = 'WooCommerce';
                $case->clientVersion->storePlatformVersion = WC_VERSION;
                $case->clientVersion->signifydClientApp = 'WooCommerce Signifyd Integration by Absolute Web';
                $case->clientVersion->signifydClientAppVersion = WC_SIGNIFYD_VERSION;

                $case_id = $this->client->createCase(apply_filters('signifyd_api_create_case', $case, $order));

                if (!$case_id) {
                    if ($this->logger) {
                        $this->logger->add('signifyd', 'Endpoint \'/cases\': ' . $this->client->getLastErrorMessage(), WC_Log_Levels::ERROR);
                    }

                    $order->add_order_note('Signifyd Case couldn\'t be created. Check integration settings and try submitting again using order actions.');

                    return false;
                }

                $order->add_meta_data('_signifyd_case_id', $case_id, true);
                $order->save_meta_data();

                $order->add_order_note('Signifyd Case #' . $case_id . ' successfully created.');

                do_action('signifyd_api_after_create_case', $order, $case_id);

                return $case_id;

            } catch (Exception $e) {
                if ($this->logger) {
                    $this->logger->add('signifyd', 'Exception in: ' . __FILE__ . ', on line: ' . __LINE__, WC_Log_Levels::ERROR);
                    $this->logger->add('signifyd', 'Exception:' . $e->__toString(), WC_Log_Levels::ERROR);
                }

                $order->add_order_note('Signifyd Case couldn\'t be created. Check integration settings and try submitting again using order actions.');

                return false;

            }
        }

        public function close_case($case_id)
        {
            return $this->client->closeCase($case_id);
        }

        private function get_purchase(WC_Order $order)
        {
            $purchase = new Purchase();
            $purchase->browserIpAddress = $order->get_customer_ip_address();
            $purchase->orderSessionId = apply_filters('signifyd_api_order_session_id', $order->get_meta('_order_session_id'), $order);
            $purchase->discountCodes = $this->get_discounts($order);
            $purchase->shipments = $this->get_shipment($order);
            $purchase->products = $this->get_products($order);
            $purchase->orderId = $order->get_order_number();
            $purchase->createdAt = $this->format_date($order->get_date_created()->getTimestamp());
            $purchase->paymentGateway = $this->get_payment_gateway($order);
            $purchase->paymentMethod = $this->get_payment_method($order);
            $purchase->transactionId = $order->get_transaction_id();
            $purchase->currency = $order->get_currency();
            $purchase->avsResponseCode = apply_filters('signifyd_api_payment_avs_response_code', $order->get_meta('_avs_code'), $order);
            $purchase->cvvResponseCode = apply_filters('signifyd_api_payment_cvv_response_code', $order->get_meta('_cvv_code'), $order);
            $purchase->totalPrice = $order->get_total();

            // Supports Phone Orders and Payments https://woocommerce.com/document/phone-orders-payments/
            $order_channel = 'yes' == $order->get_meta('_wcpop_phone_orders_payments_phone_order') ? 'PHONE' : 'WEB';
            $purchase->orderChannel = apply_filters('signifyd_api_order_channel', $order_channel, $order);

            return apply_filters('signifyd_api_purchase', $purchase, $order);
        }

        private function get_recipient(WC_Order $order)
        {
            $recipient = new Recipient();
            $recipient->fullName = $order->get_formatted_billing_full_name();
            $recipient->confirmationEmail = $order->get_billing_email();
            $recipient->confirmationPhone = $order->get_billing_phone();
            $recipient->organization = $order->get_billing_company();

            $shipping_address = new Address();
            $shipping_address->streetAddress = $order->get_shipping_address_1();
            $shipping_address->unit = $order->get_shipping_address_2();
            $shipping_address->city = $order->get_shipping_city();
            $shipping_address->provinceCode = $order->get_shipping_state();
            $shipping_address->postalCode = $order->get_shipping_postcode();
            $shipping_address->countryCode = $order->get_shipping_country();

            $recipient->deliveryAddress = $shipping_address;

            return apply_filters('signifyd_api_recipient', $recipient, $order);
        }

        private function get_card(WC_Order $order)
        {
            $card = new Card();
            $card->cardHolderName = $order->get_formatted_billing_full_name();
            $card->bin = $order->get_meta('_cc_bin');
            $card->last4 = $order->get_meta('_cc_last4');
            $card->expiryMonth = $order->get_meta('_cc_exp_month');
            $card->expiryYear = $order->get_meta('_cc_exp_year');

            $billing_address = new Address();
            $billing_address->streetAddress = $order->get_billing_address_1();
            $billing_address->unit = $order->get_billing_address_2();
            $billing_address->city = $order->get_billing_city();
            $billing_address->provinceCode = $order->get_billing_state();
            $billing_address->postalCode = $order->get_billing_postcode();
            $billing_address->countryCode = $order->get_billing_country();
            $card->billingAddress = $billing_address;

            return apply_filters('signifyd_api_card', $card, $order);
        }

        private function get_user_account(WC_Order $order)
        {
            $user_id = $order->get_customer_id();

            // guest checkout was used
            if (!$user_id) return false;

            $user = get_user_by('id', $user_id);

            $user_account = new UserAccount();
            $user_account->emailAddress = $user->user_email;
            $user_account->username = $user->user_login;
            $user_account->phone = get_user_meta($user_id, 'billing_phone', true);
            $user_account->createdDate = $this->format_date(strtotime($user->user_registered));
            $user_account->accountNumber = $user_id;
            $user_account->lastOrderId = $order->get_order_number();

            $last_update = get_user_meta($user_id, 'last_update', true);// timestamp
            if (!$last_update) {
                $last_update = $order->get_date_created()->getTimestamp();
            }
            $user_account->lastUpdateDate = $this->format_date($last_update);

            $customer_order_ids = signifyd_get_order_ids_by_user($user_id);
            $user_account->aggregateOrderCount = count($customer_order_ids);
            $user_account->aggregateOrderDollars = signifyd_sum_orders_total($customer_order_ids);

            return apply_filters('signifyd_api_user_account', $user_account, $order, $user_id, $user);
        }

        private function get_discounts(WC_Order $order)
        {
            $discounts = array();

            foreach ($order->get_items('coupon') as $coupon_item) {
                $discount = array(
                    'code' => $coupon_item->get_code()
                );

                $coupon_data = (array)$coupon_item->get_meta('coupon_data', true);

                if ('percent' === $coupon_data['discount_type']) {
                    $discount['percentage'] = floatval($coupon_data['amount']);
                } else {
                    $discount['amount'] = floatval($coupon_item->get_meta('discount_amount', true));
                }

                $discounts[] = $discount;
            }

            return apply_filters('signifyd_api_discounts', $discounts, $order);
        }

        private function get_shipment(WC_Order $order)
        {
            $shipment = new Shipment();
            $shipment->shippingPrice = floatval($order->get_shipping_total());
            $shipment->shippingMethod = apply_filters('signifyd_api_shipping_method', $shipment->shippingPrice > 0 ? 'STANDARD' : 'FREE', $order);

            return apply_filters('signifyd_api_shipment', $shipment, $order);
        }

        private function get_products(WC_Order $order)
        {
            $products = array();

            foreach ($order->get_items('line_item') as $item) {
                $_product = $item->get_product();

                $product = new Product();
                $product->itemId = $_product->get_id();
                $product->itemUrl = $_product->get_permalink();
                $product->itemName = $item->get_name();
                $product->itemIsDigital = $_product->is_virtual();
                $product->itemQuantity = $item->get_quantity();
                $product->itemPrice = $_product->get_price();

                $products[] = $product;
            }

            return apply_filters('signifyd_api_products', $products, $order);
        }

        private function get_payment_gateway(WC_Order $order)
        {
            switch ($order->get_payment_method()) {
                case 'authorize_net_aim':
                    $payment_gateway = 'Authorize.NET AIM';
                    break;
                case 'authorize_net_cim_credit_card':
                    $payment_gateway = 'Authorize.NET CIM';
                    break;
                case 'braintree_credit_card':
                case 'braintree':
                    $payment_gateway = 'Braintree';
                    break;
                case 'moneris':
                    $payment_gateway = 'Moneris';
                    break;
                case 'ppec_paypal':
                case 'paypal_express':
                case 'ppcp-gateway':
                case 'ppcp-credit-card-gateway':
                    $payment_gateway = 'PayPal';
                    break;
                case 'paypal_pro':
                    $payment_gateway = 'paypalpro';
                    break;
                case 'paypal_pro_payflow':
                    $payment_gateway = 'payflowpro';
                    break;
                case 'stripe':
                    $payment_gateway = 'stripe';
                    break;
                case 'mes_cc':
                    $payment_gateway = 'Merchant e-Solutions';
                    break;
                case 'cybsawm':
                    $payment_gateway = 'Cybersource';
                    break;
                case 'card_connect':
                    $payment_gateway = 'card_connect';
                    break;
                case 'first_data_payeezy_gateway_credit_card':
                    $payment_gateway = 'Payeezy/FirstData';
                    break;
                case 'square_credit_card':
                    $payment_gateway = 'Square';
                    break;
                default:
                    $payment_gateway = $order->get_payment_method_title();
            }

            return apply_filters('signifyd_api_payment_gateway', $payment_gateway, $order);
        }

        private function get_payment_method(WC_Order $order)
        {
            switch ($order->get_payment_method()) {
                case 'ppec_paypal':
                case 'paypal_express':
                case 'ppcp-gateway':
                case 'ppcp-credit-card-gateway':
                    $payment_method = 'PAYPAL';
                    break;
                default:
                    $payment_method = 'CREDIT_CARD';
            }

            switch ($order->get_created_via()) {
                case 'APPLE_PAY':
                case 'apple_pay':
                    $payment_method = 'APPLE_PAY';
                    break;
                case 'GOOGLE_PAY':
                case 'google_pay':
                    $payment_method = 'GOOGLE_PAY';
                    break;
            }

            return apply_filters('signifyd_api_payment_method', $payment_method, $order);
        }

        /**
         * @param $timestamp Timestamp in GMT.
         *
         * @return false|string Date formatted in ISO8601.
         */
        private function format_date($timestamp)
        {
            return gmdate('c', $timestamp);
        }

    }

}
