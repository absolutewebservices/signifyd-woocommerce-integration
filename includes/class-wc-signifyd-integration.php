<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WC_Signifyd_Integration')) {

    class WC_Signifyd_Integration extends WC_Integration
    {

        public function __construct()
        {
            $this->id = 'signifyd';
            $this->method_title = __('Signifyd');
            $this->method_description = sprintf('<p>For installation instructions please refer to our <a href="%s" target="_blank">product manual</a>. If you have questions or need assistance please <a href="%s" target="_blank">contact support</a>.</p><p><b>Supported payment gateways:</b></p>%s', 'https://www.signifyd.com/resources/manual/overview/', 'https://community.signifyd.com/support/', $this->supported_payment_gateways());
            $this->init_form_fields();
            $this->init_settings();

            $this->enabled = $this->get_option('enabled');
            $this->api_key = $this->get_option('api_key');
            $this->enable_logging = $this->get_option('enable_logging');

            add_action('woocommerce_update_options_integration_' . $this->id, array($this, 'process_admin_options'));
        }

        public function init_form_fields()
        {
            $this->form_fields = array(
                'general_title' => array(
                    'title' => __('General'),
                    'type' => 'title'
                ),
                'enabled' => array(
                    'title' => __('Enable Plugin'),
                    'label' => __('Enables the syncing of orders to Signifyd'),
                    'description' => __('Enable this settings ONLY AFTER you\'ve completed the configuration of the plugin.'),
                    'type' => 'select',
                    'default' => 'no',
                    'options' => array(
                        'no' => __('No'),
                        'test' => __('Test mode'),
                        'live' => __('Live mode')
                    )
                ),
                'api_key' => array(
                    'title' => __('API Key'),
                    'description' => sprintf('Your API key can be found on the <a href="%s" target="_blank">settings page</a> in the Signifyd console. Don\'t have an account? <a href="%s" target="_blank">Contact us</a>', 'https://app.signifyd.com/settings', 'https://www.signifyd.com/contact/'),
                    'type' => 'text',
                    'default' => ''
                ),
                'order_workflow_title' => array(
                    'title' => __('Order Workflow'),
                    'type' => 'title',
                    'description' => __('Enabling any of the order workflows below will place all newly created orders "on-hold" until Signifyd has reviewed the order.')
                ),
                'action_guarantee_approve' => array(
                    'title' => __('Approved Guarantees'),
                    'description' => __('Select what action should be taken when an order is approved for guarantee.'),
                    'type' => 'select',
                    'default' => 'no',
                    'options' => array(
                        'none' => __('Do nothing'),
                        'on-hold' => __('Set On-Hold status'),
                        'processing' => __('Set Processing status'),
                        'completed' => __('Set Completed status')
                    )
                ),
                'action_guarantee_decline' => array(
                    'title' => __('Declined Guarantees'),
                    'description' => __('Select what action should be taken when an order is declined for guarantee.'),
                    'type' => 'select',
                    'default' => 'none',
                    'options' => array(
                        'none' => __('Do nothing'),
                        'failed' => __('Set Failed status'),
                        'on-hold' => __('Set On-Hold status'),
                        'cancelled' => __('Set Cancelled status'),
                        'processing' => __('Set Processing status'),
                        'completed' => __('Set Completed status')
                    )
                ),
                'enable_order_refund' => array(
                    'title' => __('Transaction Refund/Void'),
                    'label' => __('Enables automated order refund/transaction void'),
                    'description' => __('Feature depends on the payment gateway order refund support.'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'ignored_user_roles' => array(
                    'title' => __('Ignored User Roles'),
                    'description' => __('Orders from selected user roles (groups) would be ignored from being sent to Signifyd.'),
                    'type' => 'multiselect',
                    'default' => '',
                    'class' => 'wc-enhanced-select',
                    'options' => wp_roles()->get_names()
                ),
                'delay_sending_order' => array(
                    'title' => __('Delay Sending Order'),
                    'description' => __('By default orders are sent right after the checkout has been completed and payment successfully processed.'),
                    'type' => 'number',
                    'default' => 0,
                    'placeholder' => __('Delay up to 15 minutes after the checkout'),
                    'custom_attributes' => array(
                        'min' => 0,
                        'max' => 15
                    )
                ),
                'enable_fulfillment' => array(
                    'title' => __('Enable Fulfillment'),
                    'label' => __('Enables the syncing of order fulfillment data to Signifyd'),
                    'description' => __('Feature requires WooCommerce Shipment Tracking plugin.'),
                    'type' => 'checkbox',
                    'default' => 'no'
                ),
                'webhook_url_title' => array(
                    'title' => __('Webhook URL'),
                    'type' => 'title',
                    'description' => __('Use this URL to setup your WooCommerce webhook from the Signifyd console. You MUST setup the webhook to enable order workflows and syncing of guarantees back to WooCommerce.') . sprintf('<br/><a href="%1$s" target="_blank">%1$s</a>', home_url('/signifyd/connect/api/'))
                ),
                'logging_title' => array(
                    'title' => __('Logging'),
                    'type' => 'title'
                ),
                'enable_logging' => array(
                    'title' => __('Enable Logging'),
                    'description' => __('Enables logging of errors related to the plugin for debugging and troubleshooting issues.'),
                    'type' => 'select',
                    'default' => 'no',
                    'options' => array(
                        'no' => __('No'),
                        'yes' => __('Yes')
                    )
                ),
            );
        }

        public function supported_payment_gateways()
        {
            $supported_payment_gateways = WooCommerce_Signifyd()->get_supported_payment_gateways(false);

            return '<ul><li>' . implode('</li><li>', array_values($supported_payment_gateways)) . '</li></ul>';
        }

    }

}
