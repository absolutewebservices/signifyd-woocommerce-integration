<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WC_Signifyd_Webhook_Event_Handler')) {

    class WC_Signifyd_Webhook_Event_Handler
    {

        public static function process_request($request_data)
        {
            if (!isset($request_data->caseId)) {
                return;
            }

            $orders = wc_get_orders([
                'orderby'    => 'ID',
                'order'      => 'DESC',
                'limit'      => 1,
                'meta_key'   => '_signifyd_case_id',
                'meta_value' => $request_data->caseId
            ]);

            if (empty($orders) || !current($orders) instanceof WC_Order) {
                return;
            }

            $order = current($orders);

            $has_updated = false;

            $case_score = $order->get_meta('_signifyd_case_score') ?: null;
            $guarantee_status = $order->get_meta('_signifyd_guarantee_status') ?: null;
            $review_disposition = $order->get_meta('_signifyd_review_disposition') ?: null;

            if (isset($request_data->score)
                && intval($request_data->score) !== $case_score) {
                $order->update_meta_data('_signifyd_case_score', intval($request_data->score));
                $has_updated = true;

                do_action('signifyd_case_score_update', intval($request_data->score), $order);
            }

            if (isset($request_data->guaranteeDisposition) && strtoupper($request_data->guaranteeDisposition) !== $guarantee_status) {
                $order->update_meta_data('_signifyd_guarantee_status', strtoupper($request_data->guaranteeDisposition));
                $has_updated = true;

                do_action('signifyd_guarantee_status_update', strtoupper($request_data->guaranteeDisposition), $order);
            }

            if (isset($request_data->reviewDisposition)
                && strtoupper($request_data->reviewDisposition) !== $review_disposition) {
                $order->update_meta_data('_signifyd_review_disposition', strtoupper($request_data->reviewDisposition));
                $has_updated = true;

                do_action('signifyd_review_disposition_update', strtoupper($request_data->reviewDisposition), $order);
            }

            if ($has_updated) {
                $order->save_meta_data();
                $order->save();
            }
        }

    }

}
