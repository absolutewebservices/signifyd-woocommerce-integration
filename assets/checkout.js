(function($) {

  var $form = $('form.checkout');

  if (!$form.length) {
    return;
  }

  // Authorize.Net CIM Credit Card
  $form.on('checkout_place_order_authorize_net_cim_credit_card', function() {
    $(this).find('#wc-authorize-net-cim-credit-card-account-number').attr('name', 'wc-authorize-net-cim-credit-card-account-number');
  });

  // Authorize.Net AIM Credit Card
  $form.on('checkout_place_order_authorize_net_aim', function() {
    $(this).find('#wc-authorize-net-aim-account-number').attr('name', 'wc-authorize-net-aim-account-number');
  });

}(jQuery));
