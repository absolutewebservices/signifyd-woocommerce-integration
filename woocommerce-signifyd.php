<?php
/**
 * Plugin Name: WooCommerce Signifyd Integration
 * Plugin URI: https://www.absoluteweb.com/signifyd-woocommerce-plugin/
 * Description: Guaranteed fraud and chargeback protection for WooCommerce.
 * Version: 1.4.07242024
 * Author: Absolute Web
 * Author URI: https://www.absoluteweb.com
 * Developer: Eugene Pulber
 * Developer URI: https://www.absoluteweb.com
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

define('WC_SIGNIFYD_VERSION', '1.4.07242024');
define('WC_SIGNIFYD_BASENAME', plugin_basename(__FILE__));
define('WC_SIGNIFYD_DIR_PATH', plugin_dir_path(__FILE__));
define('WC_SIGNIFYD_DIR_URL', plugin_dir_url(__FILE__));

require_once WC_SIGNIFYD_DIR_PATH . 'vendor/autoload.php';
require_once WC_SIGNIFYD_DIR_PATH . 'includes/class-wc-signifyd.php';
require_once WC_SIGNIFYD_DIR_PATH . 'includes/functions.php';

register_activation_hook(__FILE__, array('WC_Signifyd', 'on_activation'));
register_deactivation_hook(__FILE__, array('WC_Signifyd', 'on_deactivation'));

function WooCommerce_Signifyd()
{
    return WC_Signifyd::instance();
}

WooCommerce_Signifyd();
