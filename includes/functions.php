<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function signifyd_sum_orders_total($order_ids)
{
    $total = 0;

    foreach ($order_ids as $order_id) {
        $total += floatval(get_post_meta($order_id, '_order_total', true));
    }

    return $total;
}

function signifyd_get_order_ids_by_user($user_id)
{
    return wc_get_orders(array(
        'fields'   => 'ids',
        'customer' => $user_id
    ));
}
