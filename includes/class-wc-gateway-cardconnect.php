<?php

class SignifydCardConnectPaymentGateway extends CardConnectPaymentGateway
{

    public $current_order;

    /* Save the current order */

    public function get_checkout_form_data($order, $user_id)
    {

        $this->current_order = $order;

        return parent::get_checkout_form_data($order, $user_id);

    }

    /* Save the AVS and CVV values from response to the order metadata */

    public function verify_customer_data($response)
    {

        if ($this->current_order instanceof WC_Order and is_array($response)) {

            $order = $this->current_order;

            $account = '';

            if (!empty($response['account'])) {
                $account = $response['account'];
            } elseif (!empty($response['token'])) {
                $account = $response['token'];
            }

            if ($account) {
                $order->update_meta_data('_cc_last4', substr(trim($account), -4));
            }

            if (!empty($response['expiry']) and strlen($response['expiry']) === 4) {

                $month = substr($response['expiry'], 0, 2);
                $year = substr($response['expiry'], -2);

                $order->update_meta_data('_cc_exp_month', sprintf('%02d', $month));
                $order->update_meta_data('_cc_exp_year', sprintf('20%02d', $year));

            }

            if (!empty($response['binInfo']) and !empty($response['binInfo']['bin'])) {
                $order->update_meta_data('_cc_bin', wc_clean($response['binInfo']['bin']));
            }

            if (!empty($response['avsresp'])) {
                $code = strtoupper(wc_clean($response['avsresp']));
                $order->update_meta_data('_avs_code', $code);
            }

            if (!empty($response['cvvresp'])) {
                $code = strtoupper(wc_clean($response['cvvresp']));
                $order->update_meta_data('_cvv_code', $code);
            }

            if (!empty($response['retref'])) {
                $order->update_meta_data('_cardconnect_retref', wc_clean($response['retref']));
            }

            if (!empty($response['authcode'])) {
                $order->update_meta_data('_cardconnect_authcode', wc_clean($response['authcode']));
            }

            if (!empty($this->mode)) {
                $order->update_meta_data('_cardconnect_mode', $this->mode);
            }

            if (!empty($this->api_credentials)) {
                $order->update_meta_data('_cardconnect_mid', $this->api_credentials['mid']);
            }

            $order->save_meta_data();

        }

        return parent::verify_customer_data($response);

    }

    /* Capture the payment */

    public function capture_payment(WC_Order $order)
    {

        $response = array();

        $ref = $order->get_meta('_cardconnect_retref');
        $mode = $order->get_meta('_cardconnect_mode');
        $mid = $order->get_meta('_cardconnect_mid');
        $auth = $order->get_meta('_cardconnect_authcode');

        if ($ref and $mid and $mode !== 'capture') {

            $request = array(
                'retref'   => $ref,
                'merchid'  => $mid,
                'authcode' => $auth
            );

            $response = $this->get_cc_client()->captureTransaction($request);

        }

        return $response;

    }

}
